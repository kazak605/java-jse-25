package ru.kazakov.nlmk;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static ru.kazakov.nlmk.Serializer.FILE_NAME;

public class Main {
    public static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws IllegalAccessException, IOException {
        List<Object> list = new ArrayList<>();
        list.add(new Person("Sergey", "Kazakov"));
        list.add(new Person("Alex", "Popov", LocalDate.now()));
        list.add(new Person("Sergey", "Serikov", LocalDate.now(), "serikov_sy@nlmk.com"));
        Serializer serializer = new Serializer();
        serializer.writeToCSV(list);
        logger.info("File " + FILE_NAME + " was created.");
    }
}
