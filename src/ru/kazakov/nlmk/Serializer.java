package ru.kazakov.nlmk;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.util.List;

public class Serializer {

    public static final String CSV_SEPARATOR = ";";

    public static final String FILE_NAME = "list.csv";

    public void writeToCSV(List<Object> objectList) throws IllegalAccessException, IOException {
        Class<?> currentClass = checkList(objectList);
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(FILE_NAME)));
        bufferedWriter.write(changeInfo(currentClass));
        bufferedWriter.newLine();
        for (Object object : objectList) {
            StringBuilder stringBuilder = new StringBuilder();
            Class<?> objectClass = object.getClass();
            for (Field field : objectClass.getDeclaredFields()) {
                field.setAccessible(true);
                if (field.get(object) != null || field.get(object) == "") {
                    stringBuilder.append(field.get(object).toString());
                    stringBuilder.append(CSV_SEPARATOR);
                }
            }
            bufferedWriter.write(stringBuilder.toString());
            bufferedWriter.newLine();
        }
        bufferedWriter.flush();
        bufferedWriter.close();
    }

    private static Class<?> checkList(List<Object> objectList) throws IllegalAccessException {
        if (objectList.isEmpty()) throw new IllegalAccessException();
        Class<?> currentClass = objectList.get(0).getClass();
        for (Object object : objectList) {
            if (currentClass != object.getClass()) {
                throw new IllegalAccessException();
            }
        }
        return currentClass;
    }

    private static String changeInfo(Class<?> currentClass) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Field field : currentClass.getDeclaredFields()) {
            stringBuilder.append(field.getName().trim().length() == 0 ? "" : field.getName());
            stringBuilder.append(CSV_SEPARATOR);
        }
        return stringBuilder.toString();

    }
}
